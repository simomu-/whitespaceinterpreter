﻿using System;
using System.Collections.Generic;

namespace WhiteSpace {
    public class Interpreter {

        List<int> stack = new List<int>();
        Dictionary<int, int> heap = new Dictionary<int, int>();
        Dictionary<string, int> rabel = new Dictionary<string, int>();
        Stack<int> callStack = new Stack<int>();
        string sourceCode;
        int programCounter = 0;

        public void SetSourceCode(string code) {
            sourceCode = code.Replace("\r", "");
        }

        public void Run() {

            while (programCounter < sourceCode.Length - 1) {
                //char c = sourceCode[programCounter];
                int counter = programCounter;

                if(sourceCode[counter] != ' ' && 
                   sourceCode[counter] != '\n' &&
                   sourceCode[counter] != '\t') {
                    programCounter++;
                    continue;
                }

                //Stack Manipulation
                if (sourceCode[counter] == ' ') {             
                    if (sourceCode[counter + 1] == ' ') {
                        int digit = sourceCode.Substring(counter + 2).IndexOf('\n');
                        string num = sourceCode.Substring(counter + 2, digit);
                        Push(SpaceToNumeric(num, SpaceNumericType.Signed));
                        programCounter += 2 + (digit + 1);
                    }

                    if (sourceCode.Substring(counter + 1, 2) == "\n ") {
                        Duplicate();
                        programCounter += 3;
                    }

                    if (sourceCode.Substring(counter + 1, 2) == "\n\t") {
                        Roll();
                        programCounter += 3;
                    }

                    if (sourceCode.Substring(counter + 1, 2) == "\n\n") {
                        DeleteTop();
                        programCounter += 3;
                    }

                    if (sourceCode.Substring(counter + 1, 2) == "\t ") {
                        int digit = sourceCode.Substring(counter + 3).IndexOf('\n');
                        string num = sourceCode.Substring(counter + 3, digit);
                        Copy(SpaceToNumeric(num));
                        programCounter += 2 + digit;
                    }

                    if (sourceCode.Substring(counter + 1, 2) == "\t\n") {
                        int digit = sourceCode.Substring(counter + 3).IndexOf('\n');
                        string num = sourceCode.Substring(counter + 3, digit);
                        DeleteRange(SpaceToNumeric(num));
                        programCounter += 2 + digit;
                    }
                }

                // Arithmetic
                if (sourceCode.Substring(counter, 2) == "\t ") {
                    if (sourceCode.Substring(counter + 2, 2) == "  ") {
                        Addition();
                    }
                    if (sourceCode.Substring(counter + 2, 2) == " \t") {
                        Substraction();
                    }
                    if (sourceCode.Substring(counter + 2, 2) == " \n") {
                        Multiplication();
                    }
                    if (sourceCode.Substring(counter + 2, 2) == "\t ") {
                        Division();
                    }
                    if (sourceCode.Substring(counter + 2, 2) == "\t\t") {
                        Modulo();
                    }
                    programCounter += 4;
                }


                // Heap Access
                if (sourceCode.Substring(counter, 2) == "\t\t") {
                    if(sourceCode[counter + 2] == ' ') {
                        AddHeap();
                    }

                    if(sourceCode[counter + 2] == '\t') {
                        try { 
                            PushFromHeap();
                        }catch(WhiteSpaceException e) {
                            throw e;
                        }
                    }
                    programCounter += 3;
                }


                // Flow Control
                if (sourceCode[counter] == '\n') {
                    if (sourceCode.Substring(counter + 1, 2) == "  ") {
                        int digit = sourceCode.Substring(counter + 3).IndexOf('\n');
                        string num = sourceCode.Substring(counter + 3, digit);

                        AddRabel(num, programCounter + 3 + (digit + 1));
                        programCounter += 3 + (digit + 1);
                    }

                    if(sourceCode.Substring(counter + 1, 2) == " \n") {
                        int digit = sourceCode.Substring(counter + 3).IndexOf('\n');
                        string num = sourceCode.Substring(counter + 3, digit);
                        GotoRabel(num);
                    }

                    if(sourceCode.Substring(counter + 1 , 2) == "\t ") {
                        int digit = sourceCode.Substring(counter + 3).IndexOf('\n');
                        string num = sourceCode.Substring(counter + 3, digit);
                        ZeroBranch(num);
                    }

                    if (sourceCode.Substring(counter + 1, 2) == "\t\t") {
                        int digit = sourceCode.Substring(counter + 3).IndexOf('\n');
                        string num = sourceCode.Substring(counter + 3, digit);
                        NegativeBranch(num);
                    }

                    if (sourceCode.Substring(counter + 1, 2) == " \t") {
                        int digit = sourceCode.Substring(counter + 3).IndexOf('\n');
                        string num = sourceCode.Substring(counter + 3, digit);
                        StartSubroutine(num);
                    }

                    if (sourceCode.Substring(counter + 1, 2) == "\t\n") {
                        try {
                            EndSubroutine();
                        } catch (WhiteSpaceException e) {
                            throw e;
                        }
                    }

                    if (sourceCode.Substring(counter + 1, 2) == "\n\n") {
                        return;
                    }

                }


                // I/O
                if (sourceCode.Substring(counter, 2) == "\t\n") {
                    if(sourceCode.Substring(counter + 2, 2) == "  ") {
                        OutputCharactor();
                    }
                    if(sourceCode.Substring(counter + 2, 2) == " \t") {
                        OutputValue();
                    }
                    if(sourceCode.Substring(counter + 2, 2) == "\t ") {
                        InputCharactor();
                    }
                    if(sourceCode.Substring(counter + 2, 2) == "\t\n") {
                        InputValue();
                    }
                    programCounter += 4;
                }

                if(programCounter == counter) {
                    throw new WhiteSpaceException("存在しない命令が渡されました", programCounter);
                }

            }

        }

#region Stack Manipulation

        void Push(int value) {

            stack.Add(value);

        }

        void Duplicate() {

            stack.Add(stack[stack.Count - 1]);

        }

        void Roll() {
            int index = stack.Count - 1;
            int tmp = stack[index];
            stack[index] = stack[index - 1];
            stack[index - 1] = tmp;
        }

        void DeleteTop() {
            stack.RemoveAt(stack.Count - 1);
        }

        void Copy(int index) {
            stack.Add(stack[index]);
        }

        void DeleteRange(int count) {
            stack.RemoveRange(stack.Count - 1 - count, count);
        }
        #endregion

#region Arithmetic 

        void Addition() {
            int rhs = stack[stack.Count - 1];
            stack.RemoveAt(stack.Count - 1);

            int lhs = stack[stack.Count - 1];
            stack.RemoveAt(stack.Count - 1);

            int result = rhs + lhs;
            stack.Add(result);
        }

        void Substraction() {
            int rhs = stack[stack.Count - 1];
            stack.RemoveAt(stack.Count - 1);

            int lhs = stack[stack.Count - 1];
            stack.RemoveAt(stack.Count - 1);

            int result =lhs - rhs;
            stack.Add(result);
        }

        void Multiplication() {
            int rhs = stack[stack.Count - 1];
            stack.RemoveAt(stack.Count - 1);

            int lhs = stack[stack.Count - 1];
            stack.RemoveAt(stack.Count - 1);

            int result = lhs * rhs;
            stack.Add(result);
        }

        void Division() {
            int rhs = stack[stack.Count - 1];
            stack.RemoveAt(stack.Count - 1);

            int lhs = stack[stack.Count - 1];
            stack.RemoveAt(stack.Count - 1);

            int result = lhs / rhs;
            stack.Add(result);

        }

        void Modulo() {
            int rhs = stack[stack.Count - 1];
            stack.RemoveAt(stack.Count - 1);

            int lhs = stack[stack.Count - 1];
            stack.RemoveAt(stack.Count - 1);

            int result = lhs % rhs;
            stack.Add(result);
        }

        #endregion

#region Heap Access

        void AddHeap() {
            int value = stack[stack.Count - 1];
            stack.RemoveAt(stack.Count - 1);

            int address = stack[stack.Count - 1];
            stack.RemoveAt(stack.Count - 1);

            heap.Add(address, value);
        }

        void PushFromHeap() {
            int address = stack[stack.Count - 1];
            stack.RemoveAt(stack.Count - 1);

            if (heap.ContainsKey(address)){
                stack.Add(heap[address]);
            } else {
                // TODO: ヒープアクセス時のエラー処理
            }
            try {
                stack.Add(heap[address]);
            }catch(Exception e) {
                throw new WhiteSpaceException("ヒープ領域に存在しないアドレスを参照しました", programCounter , e);
            }
        }

        #endregion

#region FlowControll

        void AddRabel(string id, int pos) {
            if (rabel.ContainsKey(id)) {
                rabel[id] = pos;
            } else {
                rabel.Add(id, pos);
            }
        }

        void GotoRabel(string id) {
            if (rabel.ContainsKey(id)) {
                programCounter = rabel[id];
            } else {
                int p = 0;
                while(p < sourceCode.Length) {
                    if(sourceCode.Substring(p, 3 + id.Length + 1) == ("\n  " + id + "\n")) {
                        //int digit = sourceCode.Substring(p + 3).IndexOf('\n');
                        //string num = sourceCode.Substring(p + 3, digit);
                        programCounter = p + (3 + id.Length) + 1;
                        return;
                    } else {
                        p++;
                    }
                }

                throw new WhiteSpaceException("ラベルが存在しません", programCounter);
            }
        }

        void ZeroBranch(string id) {
            int value = stack[stack.Count - 1];
            stack.RemoveAt(stack.Count - 1);
            if(value == 0) {
                GotoRabel(id);
            } else {
                programCounter += 3 + id.Length;
            }
        }

        void NegativeBranch(string id) {
            int value = stack[stack.Count - 1];
            stack.RemoveAt(stack.Count - 1);
            if (value < 0) {
                GotoRabel(id);
            } else {
                programCounter += 3 + id.Length;
            }
        }

        void StartSubroutine(string id) {
            callStack.Push(programCounter + (3 + id.Length) + 1);
            GotoRabel(id);
        }

        void EndSubroutine() {
            try {
                programCounter = callStack.Pop();
            } catch (Exception e) {
                throw new WhiteSpaceException("サブルーチンからの戻り先が存在しません", programCounter, e);
            }
        }

#endregion

#region I/O

        void OutputValue() {
            int value = stack[stack.Count - 1];
            stack.RemoveAt(stack.Count - 1);

            Console.Write(value);
        }

        void OutputCharactor() {
            char c = (char)stack[stack.Count - 1];
            stack.RemoveAt(stack.Count - 1);

            Console.Write(c);
        }

        protected virtual void InputValue() {
            stack.Add(int.Parse(Console.ReadLine()));
        }

        protected virtual void InputCharactor() {
            stack.Add(char.Parse(Console.ReadLine()));
        }

#endregion

        int SpaceToNumeric(string spaces, SpaceNumericType type = SpaceNumericType.Unsigned) {

            int s = 1;
            if (type == SpaceNumericType.Signed && spaces.Length >= 2 && spaces[0] == '\t') {
                s = -1;
                spaces = spaces.Remove(0, 1);
            }

            spaces = spaces.Replace(' ', '0');
            spaces = spaces.Replace('\t', '1');
            spaces = spaces.Replace("\n", "");
            return Convert.ToInt32(spaces, 2) * s;
        }

    }
}
