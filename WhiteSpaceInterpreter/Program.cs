﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhiteSpace {
    class Program {
        static void Main(string[] args) {

            if(args.Length != 0) {
                StreamReader sr = new StreamReader(args[0], Encoding.GetEncoding("UTF-8"));
                string code = sr.ReadToEnd();
                WhiteSpace.Interpreter interpreter = new Interpreter();
                interpreter.SetSourceCode(code);
                interpreter.Run();
            } else {
                Console.WriteLine("WhiteSpaceInterpreter INPUT_FILE_NAME");
            }
        }
    }
}
