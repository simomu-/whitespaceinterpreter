﻿using System;

namespace WhiteSpace {

    public class WhiteSpaceException : Exception {

        int counter;

        public int programCounter {
            get { return counter; }
        }

        public WhiteSpaceException() {

        }

        public WhiteSpaceException(string message, int counter) : base(message) {
            this.counter = counter;
        }

        public WhiteSpaceException(string message, int counter, Exception inner) : base(message, inner) {
            this.counter = counter;
        }
    }

    public enum SpaceNumericType {
        Signed, Unsigned
    }
    
}
